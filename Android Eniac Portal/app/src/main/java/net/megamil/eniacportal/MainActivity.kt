package net.megamil.eniacportal

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.webkit.WebView
import android.webkit.WebViewClient
import com.google.firebase.iid.FirebaseInstanceId

class MainActivity : AppCompatActivity() {

    var mywebview: WebView? = null

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mywebview = findViewById<WebView>(R.id.webview)

        mywebview?.let {

            val token = FirebaseInstanceId.getInstance().token
            val URL = "http://jlenterprise.com.br/eniac/Home/entrada_app?token=" + token
            Log.d("URLWEBVIEW", URL)

            it.webViewClient = object : WebViewClient() {
                @Deprecated("Only Tests")
                override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                    view?.loadUrl(url)
                    return true
                }
            }
            it.settings.javaScriptEnabled = true
            it.loadUrl(URL)
        } ?: Log.w("wv","Falhou ao capturar webview")


    }
}
