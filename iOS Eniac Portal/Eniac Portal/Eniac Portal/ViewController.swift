//
//  ViewController.swift
//  Eniac Portal
//
//  Created by Eduardo dos santos on 04/04/19.
//  Copyright © 2019 Eduardo dos santos. All rights reserved.
//

import UIKit
import WebKit


class ViewController: UIViewController {
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let request = URLRequest(url: URL(string: "http://jlenterprise.com.br/eniac/Home/entrada_app")!)
        webView?.load(request)
        
    }

}

